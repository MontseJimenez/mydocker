var expect = require('chai').expect;
var $ = require('chai-jquery');
var request = require('request');

it('test suma', function() {
    expect(9+4).to.equal(13);
    console.log("prueba completada");
});

/*
it('test internet', function(done) {
    request.get("http://www.elpais.com",
                function(error, response, body) {
                  expect(response.statusCode).to.equal(200);
                  done();
    });
});
*/
describe ("Pruebas red", function() {
it('test puerto', function(done) {
  request.get("http://localhost:8081",
              function(error, response, body) {
                expect(response.statusCode).to.equal(200);
                done();
  });
});
});

it('test body', function(done) {
  request.get("http://localhost:8081",
              function(error, response, body) {
                expect(body).contains("blog");
                done();
  });
});

it('test h1', function(done) {
  request.get("http://localhost:8081",
              function(error, response, body) {
                expect(body).should.exists('<h1>\w*</h1>');
                done();
  });
});

/*
describe ('Test contenido HTML', function(){
  it('Test H1', function(done) {
  request.get("http://localhost:8081",
              function(error, response, body) {
                expect($('body h1')).to.have.text("Bienvenido a mi blog");
});
});
